--
-- File generated with SQLiteStudio v3.2.1 on Thu Sep 8 23:01:19 2022
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Customer
CREATE TABLE Customer (
    customer_id       TEXT (10) NOT NULL
                                PRIMARY KEY,
    customer_name     TEXT (50) NOT NULL,
    customer_surname  TEXT (50) NOT NULL,
    customer_tel      CHAR (12) NOT NULL,
    customer_point    INTEGER   NOT NULL,
    customer_amount   INTEGER   NOT NULL,
    customer_discount DOUBLE    NOT NULL,
    customer_type     INTEGER   NOT NULL
                                DEFAULT (1) 
);

INSERT INTO Customer (customer_id, customer_name, customer_surname, customer_tel, customer_point, customer_amount, customer_discount, customer_type) VALUES ('0001', '???????', '??????????????', '082-341-5792', 0, 1, 0.0, 2);
INSERT INTO Customer (customer_id, customer_name, customer_surname, customer_tel, customer_point, customer_amount, customer_discount, customer_type) VALUES ('0002', '????', '????', '094-312-9446', 5, 2, 5.0, 1);
INSERT INTO Customer (customer_id, customer_name, customer_surname, customer_tel, customer_point, customer_amount, customer_discount, customer_type) VALUES ('0003', '?????', '????????????', '086-256-9543', 15, 8, 15.0, 1);
INSERT INTO Customer (customer_id, customer_name, customer_surname, customer_tel, customer_point, customer_amount, customer_discount, customer_type) VALUES ('0004', '??????', '???????', '099-663-7785', 8, 3, 8.0, 1);
INSERT INTO Customer (customer_id, customer_name, customer_surname, customer_tel, customer_point, customer_amount, customer_discount, customer_type) VALUES ('0005', '????', '????????', '061-853-2031', 0, 0, 0.0, 2);
INSERT INTO Customer (customer_id, customer_name, customer_surname, customer_tel, customer_point, customer_amount, customer_discount, customer_type) VALUES ('0006', '????', '????', '065-142-0981', 6, 2, 6.0, 1);
INSERT INTO Customer (customer_id, customer_name, customer_surname, customer_tel, customer_point, customer_amount, customer_discount, customer_type) VALUES ('0007', '?????????', '?????', '098-453-7702', 0, 1, 0.0, 2);

-- Table: Employee
CREATE TABLE Employee (
    employee_id       TEXT (10) NOT NULL
                                PRIMARY KEY,
    employee_name     TEXT (50) NOT NULL,
    employee_surname  TEXT (50) NOT NULL,
    employee_date     DATE      NOT NULL,
    employee_time_in  TIME      NOT NULL,
    employee_time_oup TIME      NOT NULL,
    employee_tel      CHAR (12) NOT NULL
);

INSERT INTO Employee (employee_id, employee_name, employee_surname, employee_date, employee_time_in, employee_time_oup, employee_tel) VALUES ('63160013', '?????', '??????????', '05/09/2022', '08:00', '17:00', '063-145-6208');
INSERT INTO Employee (employee_id, employee_name, employee_surname, employee_date, employee_time_in, employee_time_oup, employee_tel) VALUES ('63160190', '????????', '???????', '05/09/2022', '08:00', '17:00', '088-154-9353');
INSERT INTO Employee (employee_id, employee_name, employee_surname, employee_date, employee_time_in, employee_time_oup, employee_tel) VALUES ('63160065', '???????', '???????', '05/09/2022', '08:00', '17:00', '062-524-8532');
INSERT INTO Employee (employee_id, employee_name, employee_surname, employee_date, employee_time_in, employee_time_oup, employee_tel) VALUES ('63160219', '??????????', '????????', '05/09/2022', '08:00', '17:00', '096-913-4526');
INSERT INTO Employee (employee_id, employee_name, employee_surname, employee_date, employee_time_in, employee_time_oup, employee_tel) VALUES ('63160204', '???????', '????????', '05/09/2022', '08:00', '17:00', '092-876-2741');

-- Table: Order
CREATE TABLE [Order] (
    order_id        TEXT (10) PRIMARY KEY
                              NOT NULL,
    store_id        TEXT (10) REFERENCES Store (store_id) ON DELETE RESTRICT
                                                          ON UPDATE RESTRICT
                              NOT NULL,
    order_item_id   TEXT (10) REFERENCES Order_item (order_item_id) ON DELETE RESTRICT
                                                                    ON UPDATE RESTRICT
                              NOT NULL,
    employee_id     TEXT (10) REFERENCES Employee (employee_id) ON DELETE RESTRICT
                                                                ON UPDATE RESTRICT
                              NOT NULL,
    customer_id     TEXT (10) REFERENCES Customer (customer_id) 
                              NOT NULL,
    order_total     DOUBLE    NOT NULL,
    order_discount  DOUBLE    NOT NULL,
    order_total_net DOUBLE    NOT NULL,
    order_cash      DOUBLE    NOT NULL,
    order_change    DOUBLE    NOT NULL,
    order_time      TIME      NOT NULL,
    order_queue     INTEGER   NOT NULL
);

INSERT INTO "Order" (order_id, store_id, order_item_id, employee_id, customer_id, order_total, order_discount, order_total_net, order_cash, order_change, order_time, order_queue) VALUES ('1', '1', '1', '63160190', '0001', 55.0, 0.0, 55.0, 100.0, 45.0, '08:30', 1);
INSERT INTO "Order" (order_id, store_id, order_item_id, employee_id, customer_id, order_total, order_discount, order_total_net, order_cash, order_change, order_time, order_queue) VALUES ('2', '1', '2', '63160204', '0002', 110.0, 5.0, 105.0, 105.0, 0.0, '08:55', 2);
INSERT INTO "Order" (order_id, store_id, order_item_id, employee_id, customer_id, order_total, order_discount, order_total_net, order_cash, order_change, order_time, order_queue) VALUES ('3', '1', '3', '63160065', '0003', 245.0, 15.0, 230.0, 500.0, 270.0, '09:25', 3);
INSERT INTO "Order" (order_id, store_id, order_item_id, employee_id, customer_id, order_total, order_discount, order_total_net, order_cash, order_change, order_time, order_queue) VALUES ('4', '1', '4', '63160190', '0004', 130.0, 8.0, 122.0, 200.0, 78.0, '09:40', 4);
INSERT INTO "Order" (order_id, store_id, order_item_id, employee_id, customer_id, order_total, order_discount, order_total_net, order_cash, order_change, order_time, order_queue) VALUES ('5', '1', '5', '63160013', '0005', 65.0, 0.0, 65.0, 100.0, 35.0, '10:22', 5);
INSERT INTO "Order" (order_id, store_id, order_item_id, employee_id, customer_id, order_total, order_discount, order_total_net, order_cash, order_change, order_time, order_queue) VALUES ('6', '1', '6', '63160190', '0006', 260.0, 6.0, 254.0, 500.0, 246.0, '10:35', 6);
INSERT INTO "Order" (order_id, store_id, order_item_id, employee_id, customer_id, order_total, order_discount, order_total_net, order_cash, order_change, order_time, order_queue) VALUES ('7', '1', '7', '63160190', '0007', 100.0, 0.0, 100.0, 100.0, 0.0, '11:00', 7);

-- Table: Order_item
CREATE TABLE Order_item (
    order_item_id        TEXT (10) NOT NULL
                                   PRIMARY KEY,
    product_id                     REFERENCES Product (product_id) ON DELETE RESTRICT
                                                                   ON UPDATE RESTRICT
                                   NOT NULL,
    order_item_id_amount NUMERIC   NOT NULL
);

INSERT INTO Order_item (order_item_id, product_id, order_item_id_amount) VALUES ('1', 4, 1);
INSERT INTO Order_item (order_item_id, product_id, order_item_id_amount) VALUES ('2', 4, 1);
INSERT INTO Order_item (order_item_id, product_id, order_item_id_amount) VALUES ('3', 7, 1);
INSERT INTO Order_item (order_item_id, product_id, order_item_id_amount) VALUES ('4', 3, 2);
INSERT INTO Order_item (order_item_id, product_id, order_item_id_amount) VALUES ('6', 10, 1);
INSERT INTO Order_item (order_item_id, product_id, order_item_id_amount) VALUES ('5', 2, 2);
INSERT INTO Order_item (order_item_id, product_id, order_item_id_amount) VALUES ('7', 1, 3);

-- Table: Product
CREATE TABLE Product (
    product_id       TEXT (10) PRIMARY KEY
                               NOT NULL,
    product_name     TEXT (50) NOT NULL,
    product_size     TEXT (5)  DEFAULT SML
                               NOT NULL,
    product_sw_level TEXT (5)  DEFAULT (123) 
                               NOT NULL,
    product_price    DOUBLE    NOT NULL,
    product_type     TEXT (5)  DEFAULT HC
                               NOT NULL,
    product_category INTEGER   DEFAULT (1) 
                               NOT NULL
);

INSERT INTO Product (product_id, product_name, product_size, product_sw_level, product_price, product_type, product_category) VALUES ('1', '????', 'SML', '0123', 45.0, 'HC', 1);
INSERT INTO Product (product_id, product_name, product_size, product_sw_level, product_price, product_type, product_category) VALUES ('2', '?????', 'SML', '0123', 60.0, 'HC', 1);
INSERT INTO Product (product_id, product_name, product_size, product_sw_level, product_price, product_type, product_category) VALUES ('3', '???????', 'SML', '0123', 45.0, 'HC', 1);
INSERT INTO Product (product_id, product_name, product_size, product_sw_level, product_price, product_type, product_category) VALUES ('5', '??????????', 'SML', '0123', 50.0, 'HC', 1);
INSERT INTO Product (product_id, product_name, product_size, product_sw_level, product_price, product_type, product_category) VALUES ('4', '?????', 'SML', '0123', 45.0, 'HC', 1);
INSERT INTO Product (product_id, product_name, product_size, product_sw_level, product_price, product_type, product_category) VALUES ('7', '??????????', 'SML', '0123', 60.0, 'HC', 1);
INSERT INTO Product (product_id, product_name, product_size, product_sw_level, product_price, product_type, product_category) VALUES ('6', '?????????', 'SML', '0123', 60.0, 'HC', 1);
INSERT INTO Product (product_id, product_name, product_size, product_sw_level, product_price, product_type, product_category) VALUES ('8', '??????', 'SML', '0123', 60.0, 'HC', 1);
INSERT INTO Product (product_id, product_name, product_size, product_sw_level, product_price, product_type, product_category) VALUES ('10', '????', '-', '-', 40.0, '-', 2);
INSERT INTO Product (product_id, product_name, product_size, product_sw_level, product_price, product_type, product_category) VALUES ('9', '???????', '-', '-', 35.0, '-', 2);

-- Table: Store
CREATE TABLE Store (
    store_id      TEXT (10) PRIMARY KEY
                            NOT NULL
                            UNIQUE,
    store_name    TEXT (50) NOT NULL,
    store_address TEXT (50) NOT NULL,
    store_tel     CHAR (12) NOT NULL
);

INSERT INTO Store (store_id, store_name, store_address, store_tel) VALUES ('1', '???? D-coffee', '???????????????????', '061-335-0274');

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
